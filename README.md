ics-ans-role-packer
===================

Ansible role to install packer (https://www.packer.io).

Requirements
------------

- ansible >= 2.4
- molecule >= 2.14

Role Variables
--------------

```yaml
packer_version: 1.7.4
packer_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/hashicorp/packer/{{ packer_version }}/packer_{{ packer_version }}_linux_amd64.zip"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-packer
```

License
-------

BSD 2-clause
