import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_packer_version(host):
    cmd = host.run('/usr/local/bin/packer version')
    assert 'Packer v1.7.4' in cmd.stdout.strip()
